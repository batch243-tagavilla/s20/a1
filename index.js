/* 1. */

let userInput = Number(prompt("Please enter a number"));
console.log("The number you provided is " + userInput);

for (count = userInput; count >= 0; count -= 5) {
    if (count <= 50) {
        console.log("The current value is at 50. Terminating the loop.");
        break;
    } else if (count % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
    } else if (count % 5 === 0) {
        console.log(count);
    }
}

/* 2. */

let myStr = "supercalifragilisticexpialidocious";
let myStrConsonant = "";

for (i = 0; i < myStr.length; i++) {
    if (myStr[i].toLowerCase() === "a" ||
        myStr[i].toLowerCase() === "e" ||
        myStr[i].toLowerCase() === "i" ||
        myStr[i].toLowerCase() === "o" ||
        myStr[i].toLowerCase() === "u") {
            continue;
    } else {
            myStrConsonant += myStr[i];
    }
}

console.log(myStr);
console.log(myStrConsonant);